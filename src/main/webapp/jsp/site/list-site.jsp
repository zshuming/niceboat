<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script type="text/javascript">

$(function (){
	ligergrid =  $("#maingrid").ligerGrid({
            columns: 
				[  
					{display : 'id',name : 'id'}, 
					{display : '名称',name : 'name',id:'name', width: "20%", align: 'left'},
					{display : '站点标题',name : 'title'}, 
					{display : '主题',name : 'theme'}, 
					{display : '默认站点',name : 'isdefault',render: function (row)
	                     {
							if (row.isdefault=="1"){
								return "是";
							}
							return "否";
	                     }
	                },
				    {
                     display: '操作', isAllowHide: false,
                     render: function (row)
                     {
                         var html ="";
                         html+='<a href="javascript:return void(0);" onclick="f_update({0})"  >设为默认站点</a>&nbsp;';
                         html+='<a href="javascript:return void(0);" onclick="f_edit({0})"  >编辑</a>&nbsp;';
  						<shiro:hasPermission name="site:delete">
                         html+='<a href="javascript:return void(0);"  onclick="f_delete({0})" >删除</a>';
                         </shiro:hasPermission >
                         return Free.replace(html,row.id);
                     }, width: 160
                 }

            ],
            url: '${ctx}/admin/site/ajax_list',
            pageSize: 100, sortName: 'id',
            width: '97%', height: '96%', 
            checkbox : false,
            pageParmName:'page',
            enabledSort:false, tree: {
                    columnId: 'name',
                    idField: 'id',
                    parentIDField: 'pid'
                }
        });
        
    });
function f_query(){
	ligergrid.set('parms',$('#query-form').serializeArray());
	ligergrid.loadData();
}
function f_edit(id){
  parent.newDialog({type:'iframe',value:'${ctx}/admin/site/input?id='+id},
		{title:"站点编辑",modal:true,width:830,height:350});
}
function f_add(){
	parent.newDialog({type:'iframe',value:'${ctx}/admin/site/input'},
		{title:"站点新增",modal:true,width:830,height:350});
}
function f_delete(id){
	  freeConfirm("是否将此信息删除?",function(){
		  Free.ajax({
	         	   url: '${ctx}/admin/site/delete.do',
	         	   data: {id:id},
	         	   success: function(data){
	         		   if (data=='ok'){
	         			 ligergrid.loadData();
	         		   }else{
	         		   	 alert("删除失败，必须先删除此站点下的所有栏目");
	         		   }
		         	}
			  });
		});
}
function f_update(id){
 Free.ajax({
   url: '${ctx}/admin/site/update.do',
   data: {id:id},
   success: function(data){
	   if (data=='ok'){
		 parent.location.reload();
	   }
   	}
  });
}
</script>
</head>
<body style="background-color: #fff">
        <table class="tab" >
          <tbody>
          <tr class="tab_white02">
            <td>     
			<form class="form-horizontal" id="query-form" method="get" action="stat-system">
					<i class="icon-search"></i>站点名称：
						<input type="text" placeholder="模糊查询..." name="search_LIKE_name">
					<button type="button" onclick="f_query();" class="btn btn-primary btn-small"><i class="icon-search  icon-white"></i> 查询</button>
					<button type="button" onclick="f_add();"  class="btn btn-primary btn-small"><i class="icon-plus icon-white"></i> 新增</button>
			</form>
            </td>
          </tr>
        </tbody></table>
        <div id="maingrid" ></div>
</body>	

</html>
