<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>

<%@ include file="header.jsp"%>
	<div class="aboutcon center">
		<div class="about_left left">
		<h1><span>${category.url}</span><br><p>${category.name}</p></h1>
		<ul class="classify">
			<h2>产品分类</h2>
			<c:forEach items="${cms:getCategory(3)}" var="it" >
				<li><a href="${ctx}/${it.url}"><b>·</b> ${it.name}</a></li>
			</c:forEach>			
		</ul>
		<h3><a href="{$CATEGORYS[11]['url']}">联系我们</a></h3>
		</div>
		<div class="about_right left">
			<h2><a href="{siteurl($siteid)}">网站首页</a> &gt; ${category.name}  &gt; 正文</h2>
			<div class="list">
			<ul>
			<c:forEach items="${cms:getArticles(categoryid,15)}" var="it" varStatus="status" >
				<div class="imglist img-wrap">
					<a href="${ctx}/aa/${it.id}.html"><img src="${it.imagesrc}"/></a><span class="text">${it.title}</span>
				</div>
			</c:forEach>
			</ul>
			<div id="pages" class="text-c"> </div>
			
			</div>
			</div>
	</div>	
</div>