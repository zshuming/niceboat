<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>

<script>

$(function(){  
    $.metadata.setType("attr", "validate");
           var v = $("form").validate({
                //调试状态，不会提交数据的
                debug: true,
                errorPlacement: function (lable, element) {

                    if (element.hasClass("l-textarea")) {
                        element.addClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().addClass("l-text-invalid");
                    }

                    var nextCell = element.parents("td:first").next("td");
                    nextCell.find("div.l-exclamation").remove(); 
                    $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
                },
                success: function (lable) {
                    var element = $("#" + lable.attr("for"));
                    var nextCell = element.parents("td:first").next("td");
                    if (element.hasClass("l-textarea")) {
                        element.removeClass("l-textarea-invalid");
                    }
                    else if (element.hasClass("l-text-field")) {
                        element.parent().removeClass("l-text-invalid");
                    }
                    nextCell.find("div.l-exclamation").remove();
                },
                submitHandler: function (form) {
                  form.submit();
                }
            });
            
    if ($("#categoryid").val()=="")
    	$("#categoryid").val("${param.categoryid }");
    	
    $("form").ligerForm({inputWidth:470});
	
	//CKEDITOR.replace('content',{toolbar:'Full', skin : 'kama', height:500});  
});


</script>

</head>
<body>
	<form id="inputForm" method="post" action="${ctx}/admin/article/save" class="form-horizontal">
	
 
		<input type="hidden" name="id" value="${ob.id }" >	
		<table cellpadding="0" cellspacing="0" class="l-table-edit" >
            <tr>
                <td align="right" class="l-table-edit-td">栏目id:</td>
                <td align="left" class="l-table-edit-td" >
                	<input type="text" name="categoryid" id="categoryid" value="${ob.categoryid}" validate="{required:true}"  >
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">优先级(数值高越置顶):</td>
                <td align="left" class="l-table-edit-td" >
                	<input type="text" name="sx"  value="${ob.sx}" >
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">标题:</td>
                <td align="left" class="l-table-edit-td" >
                	<input type="text" class="form-control" id="title" name="title" value="${ob.title }" validate="{required:true}" />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" class="l-table-edit-td">关键字:</td>
                <td align="left" class="l-table-edit-td" >
                	<input type="text" class="form-control" name="description" value="${ob.description}"  />
                </td>
                <td align="left"></td>
            </tr>
            <c:if test="${category.showMode==3}">
	            <tr>
	                <td align="right" class="l-table-edit-td">缩略图:</td>
	                <td align="left" class="l-table-edit-td" >
	             		<input type="hidden" id="imagesrc" name="imagesrc" value="${ob.imagesrc}" />
						<tags:ckfinder input="imagesrc" type="images" uploadPath="/cms/article" selectMultiple="false"/>
	                </td>
	                <td align="left"></td>
	            </tr>
			</c:if>
            <tr>
                <td align="right" class="l-table-edit-td">文章内容:</td>
                <td align="left" class="l-table-edit-td" >
                </td>
                <td align="left"></td>
            </tr>
            </table>
			
								 <textarea class="ckeditor" id="content" name="content" >${articleData.content }</textarea> 
			<div id="contact">
							<button type="submit" >保存</button>
			</div>		
				</form>
</body>	
<script type="text/javascript">
var contentCkeditor = CKEDITOR.replace("content");
contentCkeditor.config.height = "";//
contentCkeditor.config.ckfinderPath="${ctx}/static/ckfinder";
var date = new Date(), year = date.getFullYear(), month = (date.getMonth()+1)>9?date.getMonth()+1:"0"+(date.getMonth()+1);
contentCkeditor.config.ckfinderUploadPath="/cms/article/"+year+"/"+month+"/";//
</script>
</html>