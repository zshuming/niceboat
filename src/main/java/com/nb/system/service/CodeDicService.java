package com.nb.system.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.common.service.BaseService;
import com.nb.system.model.CodeDicEntity;

@Service
public class CodeDicService extends BaseService{
	
	@Autowired
	private Dao dao;
		
	public  Map<String, String> getRealCodeMapBySection(String section) {
		List<CodeDicEntity> list = dao.query(CodeDicEntity.class, Cnd.where("section","=",section));
		Map<String, String> codeMap = new LinkedHashMap<String, String>();
		for(CodeDicEntity cde:list){
			codeMap.put(cde.getCode(), cde.getCodename());
		}
		return codeMap;
	}
	
	public  Map<String, String> getRealCodeMapBySectionName(String sectionname) {
		List<CodeDicEntity> list = dao.query(CodeDicEntity.class, Cnd.where("sectionname","=",sectionname));
		Map<String, String> codeMap = new LinkedHashMap<String, String>();
		for(CodeDicEntity cde:list){
			codeMap.put(cde.getCode(), cde.getCodename());
		}
		return codeMap;
	}
}
