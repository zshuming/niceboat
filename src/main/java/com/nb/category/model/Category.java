﻿package com.nb.category.model;

import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
/**
 * 栏目
 * @author mefly
 *
 */
@Table("t_cms_category") 
public class Category {
	@Id
    private Long id;

    private String name;//栏目名称
    private String image;
    private String url;
    private Long pid;
    private String showMode;//显示方式   1列表  2第一条内容
    private String siteid;//显示方式   1列表  2第一条内容
    private String href;//链接
    private int openwindow;//打开新窗口
    
    //---------------------------
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public String getShowMode() {
		return showMode;
	}
	public void setShowMode(String showMode) {
		this.showMode = showMode;
	}
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public int getOpenwindow() {
		return openwindow;
	}
	public void setOpenwindow(int openwindow) {
		this.openwindow = openwindow;
	}

}